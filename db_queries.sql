UPDATE mysql.user SET Password=PASSWORD('Erebus26') WHERE User='root';

CREATE TABLE cueball_events
(
    ID BIGINT NOT NULL AUTO_INCREMENT,
    Type VARCHAR(100),
    Message  VARCHAR(2000),
    LastUpdated TIMESTAMP NOT NULL,
    Timestamp BIGINT NOT NULL,
    Metadata  VARCHAR(200),
    SessionID BIGINT NOT NULL,
    UserID BIGINT NOT NULL,
    CONSTRAINT ck_events PRIMARY KEY (SessionID),
    CONSTRAINT ck_id UNIQUE (ID)
);
